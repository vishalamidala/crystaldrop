import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class BodyComponent  {

 pure = 'CystalDrop Mineralized Drinking Water is processed in the most advanced plant with zero human touch. It is treated through a multiple process of purification so that the quality of water conforms or exceeds to the Packaged Drinking Water Quality Standards (PDWQS) as specified by Food Safety and Standards Authority of India (FSAI). Our water filtration process is ISI certified.';
 safe = 'CystalDrop Mineralized Drinking Water is the safest in the world. The safety comes from an inbuilt ability to verify the quality, source, mineral composition and batch level test reports of every single dispenser via a QR Code.';
 easy = 'CystalDrop Mineralized Drinking Water comes with a way of tracking Your delivery status.';
}
